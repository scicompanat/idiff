#!/bin/sh

# Run a strict delinting using pylint

# this page is helpful for looking up error codes and meanings
# http://pylint-messages.wikidot.com/all-codes

PYLINTIGNORE=C0103,R0912,R0913,R0914,R0915,W0511,R0903,R0904,I0011
PYLINTCMD="pylint --include-ids=y --reports=n -d $PYLINTIGNORE"

if [[ $# -gt 0 ]]
then
    $PYLINTCMD $*
else
    find . -name \*.py | xargs $PYLINTCMD
fi
