IDiff: irrotational diffeomorphisms for computational anatomy
-------------------------------------------------------------

This is (so far) an implementation of irrotational and hybrid image
registration.  I will put atlas building and regression in here eventually.
