#!/usr/bin/env python2
"""Setup script for IDiff package"""

from distutils.core import setup

with open('README.txt') as file:
    # load the description from README so that bitbucket and PyPI pages match
    long_description = file.read()

    setup(name='IDiff',
        version='0.01',
        description='Irrotational diffeomorphism package using PyCA',
        author='Jacob Hinkle',
        author_email='jacob@sci.utah.edu',
        url='http://bitbucket.org/scicompanat/idiff',
        packages=['IDiff'],
        long_description=long_description)
