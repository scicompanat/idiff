#!/usr/bin/python2
"""Example of matching two "C" images using hybrid IDiff+SDiff warping"""

# NOTE: This is only necessary if you do not have IDiff installed somewhere that
# python can find it.  Developers: your program script should not contain these
# lines, which are only necessary to avoid
#   ImportError: No module named IDiff.*
# messages
import os.path
import sys
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
# END example-specific path setup

import AppUtils.Config as cfg
import AppUtils.Compute as comp

import IDiff.Atlas

import PyCA.Core as ca
import PyCA.Common as common

import numpy as np

import math


def makeC(N, r1, r2, t1, t2, mType=ca.MEM_HOST, blurSigma=0.):
    """Create the infamous C shaped test images"""
    # we'll populate a numpy array first
    Il = np.zeros([N, N])

    for x in xrange(N):
        for y in xrange(N):
            xw = (x - N/2.)/float(N)
            yw = (y - N/2.)/float(N)
            # get radius
            r = math.sqrt(xw*xw+yw*yw)
            # get theta
            t = math.atan2(yw, -xw)
            if (t1 <= t <= t2) and (r1 <= r <= r2):
                Il[x, y] = 1.

    # blur
    print "blurSigma=", blurSigma
    Il = common.GaussianBlur(Il.T, blurSigma)

    # convert to Image3D
    return common.ImFromNPArr(Il, mType=mType)


# start with a default IDiff Matching config then customize
cf = cfg.SpecToConfig(IDiff.Atlas.AtlasConfigSpec)

# Either ca.MEM_HOST or ca.MEM_DEVICE: determines whether we run on GPU
#useCUDA = ca.GetNumberOfCUDADevices() > 0
useCUDA = False  # force CPU computation

# Computation parameters
cf.compute.useCUDA = useCUDA
cf.compute.numProcesses = 4  # set higher AND set useMPI=True for parallelism
cf.compute.useMPI = True
#cf.compute.hostList = ['localhost']  # list of hosts for MPI
cf.compute.hostList = None
cf.compute.interactive = True  # we want to see some output right?

mType = ca.MEM_DEVICE if useCUDA else ca.MEM_HOST

# Example image parameters
N = 256
r1 = .15
r2 = .35
t1 = -math.pi*.75   # start angle
t2 = 0              # end angle for I0
t3 = math.pi*.5    # end angle for I1
imageSigma = N/40.  # smooth images

nPt = 3 # images, evenly distributed

cf.study.subjectData = [makeC(N, r1, r2, t1, t2+j*(t3-t2)/(nPt-1), mType,
                        blurSigma=imageSigma) for j in xrange(nPt)]

# template regularization
# NOTE: if you set templateRegWeight = 0, we will use closed-form template
# update using splatting with division by interpolation weights, which closely
# approximates the optimal template
cf.study.templateRegWeight = 0.5

# IDiff parameters (does not depend on optimization)
cf.idiff.symmetric = False
cf.idiff.sdiffScaling = 10.0
cf.idiff.regWeight = 100.

# Optimization parameters
cf.optim.Niter = 50000
cf.optim.stepSize = .001

# IO parameters (stuff like plotting and file output)
cf.io.plotEvery = 1000
cf.io.saveEvery = 10
cf.io.quiverEvery = N/64
cf.io.outputPrefix = os.path.join("output", "atlas_c", '')

# print out the setup
print(cfg.ConfigToYAML(IDiff.Atlas.AtlasConfigSpec, cf))

# This runs the algorithm
comp.Compute(IDiff.Atlas.BuildAtlas, cf)
