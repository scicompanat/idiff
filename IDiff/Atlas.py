#!/usr/bin/python2
"""Atlas building using irrotational diffeomorphisms"""

# PyCA stuff
import PyCA.Core as ca
import PyCA.Common as common
import PyCA.Display as display

# generic apputils stuff
from AppUtils import Config, Optim, Compute

# IDiff Core
import Core
import Parallel

import numpy as np

try:
    import matplotlib.pyplot as plt
except:
    print "Warning: matplotlib import failed, some functionality disabled"

import mpi4py

import os
import sys
import time


# Set up our configuration parameters (used by ConfigFiles.Validate)
def _valspecregressionstudy(c):
    """Atlas-specific study config validation"""
    assert len(c.subjectData) > 0, "Must provide some image files"
StudySpec = {
    'subjectData':
    Config.Param(default=['subject1.mhd', 'subject2.mhd'],
                 required=True,
                 comment="List of subject image files"),
    'templateRegWeight':
    Config.Param(default=0.0,
                 required=False,
                 comment="H1 regularization term for template image"),
    'templateStepSize':
    Config.Param(default=0.1,
                 required=False,
                 comment="Step size for template gradient descent"),
    '_validation_hook': _valspecregressionstudy}
# full atlas building config spec
AtlasConfigSpec = {
    'compute': Compute.ComputeConfigSpec,
    'idiff': Core.IDiffConfigSpec,
    'optim': Optim.OptimConfigSpec,
    'study': StudySpec,
    'io': {
        'saveEvery':
        Config.Param(default=0,
                     comment="How often to save output volumes, energy, etc."),
        'plotEvery':
        Config.Param(default=10,
                     comment="Update plots every N iterations"),
        'plotSlice':
        Config.Param(default=None,
                     comment="Slice to plot.  Defaults to mid axial"),
        'outputPrefix':
        Config.Param(default="./",
                     comment="Where to put output.  Don't forget trailing "
                     + "slash")},
    '_resource': 'IDiff_Atlas'}


def BuildAtlas(cf):
    """
    A single worker just holds the base image, a set of data and deformations
    for a predetermined subset of the overall data.
    """
    localRank = Compute.GetMPIInfo()['local_rank']
    rank = Compute.GetMPIInfo()['rank']

    # prepare output directory
    common.Mkdir_p(os.path.dirname(cf.io.outputPrefix))

    # just one reporter process on each node
    isReporter = rank == 0

    if isReporter:
        # Output loaded config
        if cf.io.outputPrefix is not None:
            cfstr = Config.ConfigToYAML(AtlasConfigSpec, cf)
            with open(cf.io.outputPrefix + "config.yaml", "w") as f:
                f.write(cfstr)

    # subdivide data, our subset is called nodeFiles
    nodeData = cf.study.subjectData[rank::cf.compute.numProcesses]
    nodeNums = range(len(cf.study.subjectData))[rank::cf.compute.numProcesses]

    # initialize host memory to hold data and local deformations
    J = [common.LoadITKImage(f, ca.MEM_HOST)
         if isinstance(f, str) else f for f in nodeData]

    # get imGrid from data
    imGrid = J[0].grid()
    # mem type is determined by whether or not we're using CUDA
    mType = ca.MEM_DEVICE if cf.compute.useCUDA else ca.MEM_HOST

    # allocate outputs in host memory
    phiinv = [ca.Field3D(imGrid, ca.MEM_HOST) for f in nodeData]
    detDphiinv = [ca.Image3D(imGrid, ca.MEM_HOST)
                  for f in nodeData]
    for i in xrange(len(J)):  # initialize parameters
        ca.SetToIdentity(phiinv[i])
        ca.SetMem(detDphiinv[i], 1.0)

    # need some host memory in np array format for MPI reductions
    if cf.compute.useMPI:
        mpiBuf = None if mType == ca.MEM_HOST else ca.Image3D(imGrid,
                                                              ca.MEM_HOST)

    # start up the memory manager for scratch variables
    ca.ThreadMemoryManager.init(imGrid, mType, 0)

    # Set up a diffOp for scalar laplacian and inverse
    if mType is ca.MEM_HOST:
        diffOp = ca.FluidKernelFFTCPU()
    else:
        diffOp = ca.FluidKernelFFTGPU()
    diffOp.setGrid(imGrid)
    diffOp.setAlpha(1.0)
    diffOp.setBeta(0.0)
    diffOp.setGamma(0.0)

    # Set up base image
    I0 = ca.ManagedImage3D(imGrid, mType)

    # initialize image to mean
    ca.SetMem(I0, 0)
    tmp = ca.ManagedImage3D(imGrid, mType)
    for Ji in J:
        # copy to preallocated memory (this is faster for GPU, slower for CPU)
        ca.Copy(tmp, Ji)
        I0 += tmp
    del tmp
    if cf.compute.useMPI:
        Parallel.Reduce(I0, mpiBuf)

    # divide by total num subjects, NOT len(J)
    I0 /= len(cf.study.subjectData)

    # start the timer
    startTime = time.time()

    E = []
    for it in xrange(cf.optim.Niter):
        # reset sum variables
        if cf.study.templateRegWeight > 0.0:  # gradient descent, just hold grad
            splatSum = ca.ManagedImage3D(imGrid, mType)
            ca.SetMem(splatSum, 0)
        else:  # closed form
            splatSum = ca.ManagedImage3D(imGrid, mType)
            splatOnesSum = ca.ManagedImage3D(imGrid, mType)
            ca.SetMem(splatSum, 0)
            ca.SetMem(splatOnesSum, 0)

        Ei = 0.0
        for i in xrange(len(J)):
            # set up temporaries to hold arguments to Step
            Ij = ca.ManagedImage3D(imGrid, mType)
            detDphiinvj = ca.ManagedImage3D(imGrid, mType)
            phiinvj = ca.ManagedField3D(imGrid, mType)

            # copy from host mem to temps on device
            ca.Copy(Ij, J[i])
            ca.Copy(phiinvj, phiinv[i])
            ca.Copy(detDphiinvj, detDphiinv[i])

            # run methods from IDiffMatching to do actual heavy lifting
            phiI0 = ca.ManagedImage3D(imGrid, mType)
            diff = ca.ManagedImage3D(imGrid, mType)

            # populate phiI0, diff and compute energy
            SSE = Core.MatchTerm(I0, phiinvj, detDphiinvj, phiI0, diff, Ij,
                                 cf.idiff)
            regE = Core.RegularizationTerm(detDphiinvj)

            # Take a single optimization step
            Core.Step(phiI0, diff, phiinvj, detDphiinvj, diffOp, cf.idiff,
                      cf.optim)

            # free up some memory
            del phiI0
            #del diff

            # return the objective function before we took a step
            Ej = (0.5*SSE + 4.*cf.idiff.regWeight*regE) / float(I0.nVox())
            Ei += Ej

            # Hold on to updated phiinv, jacobian
            ca.Copy(phiinv[i], phiinvj)
            ca.Copy(detDphiinv[i], detDphiinvj)
            del detDphiinvj

            if cf.study.templateRegWeight > 0:
                splat = ca.ManagedImage3D(imGrid, mType)
                ca.Splat(splat, phiinvj, diff)
                splatSum += splat
            else:
                # splat back with updated phi (and ones)
                splat = ca.ManagedImage3D(imGrid, mType)
                ca.Splat(splat, phiinvj, Ij)
                splatSum += splat
                ca.SetMem(Ij, 1.0)
                ca.Splat(splat, phiinvj, Ij)
                splatOnesSum += splat

            del phiinvj, Ij

        # if there are multiple nodes we'll need to sum across processes now
        if cf.compute.useMPI:
            # do an MPI sum
            if cf.study.templateRegWeight > 0:
                Parallel.Reduce(splatSum, mpiBuf)
            else:
                Parallel.Reduce(splatSum, mpiBuf)
                Parallel.Reduce(splatOnesSum, mpiBuf)

            # also sum up energies of other nodes
            Ei = np.array([Ei])  # i don't know how to reduce over a scalar
            mpi4py.MPI.COMM_WORLD.Allreduce(mpi4py.MPI.IN_PLACE,
                                            Ei,
                                            op=mpi4py.MPI.SUM)
            Ei = Ei[0]

        # Template update
        if cf.study.templateRegWeight > 0: # gradient descent on the template
            # smooth splatSum
            scratchV = ca.ManagedField3D(imGrid, mType)
            ca.Copy(scratchV, splatSum, 0)
            diffOp.setDivergenceFree(False)
            diffOp.applyInverseOperator(scratchV)
            ca.Copy(splatSum, scratchV, 0)

            # add template Reg part
            ca.Add_MulC_I(splatSum, I0, cf.study.templateRegWeight)

            # do the GD step
            ca.Add_MulC_I(I0, splatSum,
                          -cf.optim.stepSize*cf.study.templateStepSize)
        else:  # "closed-form" update
            # now divide to get the new base image
            ca.Div(I0, splatSum, splatOnesSum)

        # keep track of energy in this iteration
        E.append(Ei)

        if isReporter and cf.io.plotEvery > 0 \
            and it % cf.io.plotEvery == cf.io.plotEvery - 1:
            plt.figure("template")
            plt.clf()
            display.DispImage(I0, newFig=False, sliceIdx=cf.io.plotSlice)
            plt.draw()
            plt.show()

            plt.figure("phiinv0")
            plt.clf()
            display.GridPlot(phiinv[0], sliceIdx=cf.io.plotSlice,
                             plotBase=False, every=2)
            plt.draw()
            plt.show()

            plt.figure("detDphiinv0")
            plt.clf()
            display.DispImage(detDphiinv[0], newFig=False, sliceIdx=cf.io.plotSlice, cmap='jet')
            plt.draw()
            plt.show()

        if it % cf.io.saveEvery == cf.io.saveEvery - 1 \
            or it == (cf.optim.Niter-1):
            # save output
            for nabs, pij, detj in zip(nodeNums, phiinv, detDphiinv):
                common.SaveITKField(pij, cf.io.outputPrefix +
                                    "phiinv" + str(nabs) + ".mhd")
                common.SaveITKImage(detj, cf.io.outputPrefix +
                                    "detDphiinv" + str(nabs) + ".mhd")
            if isReporter:
                common.SaveITKImage(I0, cf.io.outputPrefix + "I0.mhd")

        if isReporter:
            print "Iter", it, "of", cf.optim.Niter, " E =", Ei, " t =", \
                time.time() - startTime
            # output energy to text file for easier plotting
            with open(cf.io.outputPrefix + "energy.txt", 'w') as f:
                f.writelines('\n'.join(map(str, E)))

        # TODO: output every Nth iteration (base image only for one process)
    return E


if __name__ == '__main__':
    try:
        usercfg = Config.Load(spec=AtlasConfigSpec, argv=sys.argv)
    except Config.MissingConfigError:
        # Don't freak out, this should have printed a sample config to STDOUT
        sys.exit(1)

    Compute.Compute(BuildAtlas, usercfg)
