"""Parallelization helper functions for PyCA"""

from mpi4py import MPI

import PyCA.Core as ca


def Reduce(A, hA, op=MPI.SUM):
    """Reduce PyCA Image3D or Field3D over MPI

    A can live anywhere but hA needs to be of mType MEM_HOST

    """
    if A.memType() == ca.MEM_HOST:
        # can do this in place without using hA
        MPI.COMM_WORLD.Allreduce(MPI.IN_PLACE, A.asnp(), op=op)
    else:
        # will need some uploading and downloading
        assert(hA.memType() == ca.MEM_HOST)  # make sure we can actually use hA
        ca.Copy(hA, A)  # download
        MPI.COMM_WORLD.Allreduce(MPI.IN_PLACE, hA.asnp(), op=op)
        ca.Copy(A, hA)  # upload

