#!/usr/bin/python2
"""Image matching using irrotational diffeomorphisms"""

from AppUtils import Config, Optim, Compute
import Core

import PyCA.Core as ca
import PyCA.Common as common
import PyCA.Display as display

try:
    import matplotlib.pyplot as plt
except:
    print "Warning: matplotlib import failed, some functionality disabled"

import os
import sys


StudySpec = {
    'I0':
    Config.Param(default='subject1.mhd', required=True,
                    comment="Initial (moving) image file"),
    'I1':
    Config.Param(default='subject2.mhd', required=True,
                    comment="Target (fixed) image file")}
MatchingConfigSpec = {
    'compute': Compute.ComputeConfigSpec,
    'idiff': Core.IDiffConfigSpec,
    'study': StudySpec,
    'optim': Optim.OptimConfigSpec,
    'io': {
        'plotEvery':
        Config.Param(default=10,
                     comment="Update plots every N iterations"),
        'plotSlice':
        Config.Param(default=None,
                     comment="Slice to plot.  Defaults to mid axial"),
        'quiverEvery':
        Config.Param(default=1,
                     comment="How much to downsample for quiver plots"),
        'outputPrefix':
        Config.Param(default="./",
                     comment="Where to put output.  Don't forget trailing "
                     + "slash")},
    '_resource': 'IDiff_Matching'}


def MatchingIteration(I0, phiinv, sqrtDphiinv, I1, diffOp, cfIDiff, cfOptim):
    """Perform a single full optimization iteration.

    Return the objective function value BEFORE the step

    """
    grid = I0.grid()
    mType = I0.memType()
    # temporaries
    phiI0 = ca.ManagedImage3D(grid, mType)
    diff = ca.ManagedImage3D(grid, mType)

    # data match energy (respects symmetric)
    SSE = Core.MatchTerm(I0, phiinv, sqrtDphiinv, phiI0, diff, I1, cfIDiff)
    regE = Core.RegularizationTerm(sqrtDphiinv)

    # Take a single optimization step
    Core.Step(phiI0, diff, phiinv, sqrtDphiinv, diffOp, cfIDiff, cfOptim)

    # return the objective function before we took a step
    return (0.5*SSE + 4.*cfIDiff.regWeight*regE) / float(I0.nVox())


def MatchingPlots(I0, phiinv, sqrtDphiinv, I1, E, cfIO, grayrange=None):
    """Do some summary plots for image matching"""
    # for purposes of plotting, we need to square detDphi
    ca.Sqr_I(sqrtDphiinv)  # undo the sqrt (Don't forget to resqrt!)
    fig = plt.figure('I0')
    plt.clf()
    fig.patch.set_facecolor('white')
    display.DispImage(I0, None, newFig=False, cmap='gray', rng=grayrange,
                      sliceIdx=cfIO.plotSlice)
    if cfIO.outputPrefix is not None:
        plt.savefig(cfIO.outputPrefix + 'I0.png')

    fig = plt.figure('I1')
    plt.clf()
    fig.patch.set_facecolor('white')
    display.DispImage(I1, None, newFig=False, cmap='gray', rng=grayrange,
                      sliceIdx=cfIO.plotSlice)
    if cfIO.outputPrefix is not None:
        plt.savefig(cfIO.outputPrefix + 'I1.png')

    fig = plt.figure('phiinv')
    plt.clf()
    fig.patch.set_facecolor('white')
    display.GridPlot(phiinv, every=cfIO.quiverEvery, color='k',
                     sliceIdx=cfIO.plotSlice, isVF=False, plotBase=False)
    plt.axis('equal')
    plt.axis('off')
    if cfIO.outputPrefix is not None:
        plt.savefig(cfIO.outputPrefix + 'phiinvgrid.pdf')

    fig = plt.figure('detDphiinv')
    plt.clf()
    fig.patch.set_facecolor('white')
    display.DispImage(sqrtDphiinv, None, newFig=False,
                      sliceIdx=cfIO.plotSlice, cmap='jet')
    plt.colorbar()
    if cfIO.outputPrefix is not None:
        plt.savefig(cfIO.outputPrefix + 'detDphiinv.pdf')

    #fig = plt.figure('diff')
    #plt.clf()
    #fig.patch.set_facecolor('white')
    #display.DispImage(diff, None, newFig=False, rng=grayrange,
                      #cmap='gray', sliceIdx=cfIO.plotSlice)
    #plt.draw()
    #plt.show()
    #if outputPrefix is not None:
        #plt.savefig(cfIO.outputPrefix + 'diff.png')

    fig = plt.figure('energy')
    fig.patch.set_facecolor('white')
    plt.plot(E)
    plt.draw()
    plt.show()
    if cfIO.outputPrefix is not None:
        plt.savefig(cfIO.outputPrefix + 'energy.pdf')

    # output some volumes to do postprocessing with.  ca.ITKFileIO is deprecated, use common.SaveITKImage

    #ca.ITKFileIO.SaveImage(outputPrefix + 'phiI0.mhd', p.phiI0)
    #ca.ITKFileIO.SaveImage(cfIO.outputPrefix + 'detDphiinv.mhd',
    #                        sqrtDphiinv)
    #ca.ITKFileIO.SaveField(cfIO.outputPrefix + 'phiinv.mhd', phiinv)
     
    common.SaveITKImage(sqrtDphiinv, cfIO.outputPrefix + 'detDphiinv.mhd' )
    common.SaveITKField(phiinv, cfIO.outputPrefix + 'phiinv.mhd' )


    # need |Dphi| to go back to sqrt now
    ca.Sqrt_I(sqrtDphiinv)


def Matching(cf):
    """Run IDiff (with optional SDiff piece) matching"""

    # prepare output directory
    common.Mkdir_p(os.path.dirname(cf.io.outputPrefix))

    # Output loaded config
    if cf.io.outputPrefix is not None:
        cfstr = Config.ConfigToYAML(MatchingConfigSpec, cf)
        with open(cf.io.outputPrefix + "config.yaml", "w") as f:
            f.write(cfstr)

    mType = ca.MEM_DEVICE if cf.compute.useCUDA else ca.MEM_HOST

    # load data
    I0 = common.LoadITKImage(cf.study.I0, mType) \
        if isinstance(cf.study.I0, str) else cf.study.I0
    I1 = common.LoadITKImage(cf.study.I1, mType) \
        if isinstance(cf.study.I1, str) else cf.study.I1

    grid = I0.grid()
    mType = I0.memType()

    # set up memory manager
    # 4 pools initially: 1 image and 1 field
    ca.ThreadMemoryManager.init(grid, mType, 4)

    # set up outputs
    # (might as well use managed memory in case this is inside an import)
    phiinv = ca.ManagedField3D(grid, mType)
    sqrtDphiinv = ca.ManagedImage3D(grid, mType)

    # initial estimate
    ca.SetToIdentity(phiinv)
    ca.SetMem(sqrtDphiinv, 1.0)

    # Set up a diffOp for scalar laplacian and inverse
    if mType is ca.MEM_HOST:
        diffOp = ca.FluidKernelFFTCPU()
    else:
        diffOp = ca.FluidKernelFFTGPU()
    diffOp.setGrid(grid)
    diffOp.setAlpha(1.0)
    diffOp.setBeta(0.0)
    diffOp.setGamma(0.0)

    # get the grayscale range from I0, I1
    grayrange = [min(ca.Min(I0), ca.Min(I1)), max(ca.Max(I0), ca.Max(I1))]

    E = []  # holds a list of energies for plotting convergence
    for it in xrange(cf.optim.Niter):
        print 'Iter', it, 'of', cf.optim.Niter,

        # the following does the forward apply of I0, computation of gradient,
        # and a gradient step
        Ei = MatchingIteration(I0, phiinv, sqrtDphiinv, I1, diffOp, cf.idiff,
                               cf.optim)
        E.append(Ei)

        print 'E =', Ei
        # plot some stuff
        if cf.io.plotEvery > 0 and (((it + 1) % cf.io.plotEvery) == 0
                                    or it == cf.optim.Niter - 1):
            MatchingPlots(I0, phiinv, sqrtDphiinv, I1, E, cf.io,
                          grayrange=grayrange)
            raw_input('Plotted.  Press Enter to continue.')


if __name__ == '__main__':
    try:
        usercfg = Config.Load(spec=MatchingConfigSpec, argv=sys.argv)
    except Config.MissingConfigError:
        # Don't freak out, this should have printed a sample config to STDOUT
        sys.exit(1)

    #PyCACompute.Compute(Matching, usercfg)  # use MPI possibly?
    Matching(usercfg)  # single thread only
