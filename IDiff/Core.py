"""Core components of IDiff algorithms"""

from AppUtils import Config

import PyCA.Core as ca


def _valspecidiff(c):
    """IDiff-specific config validation"""
    assert not c.symmetric, "Symmetric match term not yet supported"
IDiffConfigSpec = {
    'regWeight':
    Config.Param(default=0.01,
                 required=True,
                 comment="Regularization (IDiff geodesic distance) term "
                 + "weight"),
    'sdiffScaling':
    Config.Param(default=1.0,
                 comment=
                 "Scaling for free SDiff steps (multiplied by step size "
                 + "also). Set to zero to enforce IDiff-only updates."),
    'symmetric':
    Config.Param(default=False,
                 comment="Use symmetric image match using sqrt Jacobian"),
    '_validation_hook': _valspecidiff}


def MatchTerm(I0, phiinv, sqrtDphiinv, phiI0, diff, I1, cfidiff):
    """Compute the image difference by pushing forward I0 and subtracting I1

    cfidiff should be a config section conforming to IDiffConfigSpec
    """
    grid = I0.grid()
    mType = I0.memType()

    # Compute phi.I0(y) = I0(phiinv(y))
    ca.ApplyH(phiI0, I0, phiinv)

    # get difference image
    ca.Sub(diff, phiI0, I1)

    # compute and report error
    if cfidiff.symmetric:
        # grab a temp image
        diffSq = ca.ManagedImage3D(grid, mType)
        ca.MulMul(diffSq, diff, diff, sqrtDphiinv)
        SSE = ca.Sum2(diffSq)
        del diffSq
    else:
        # Sum diff squared to compute energy
        SSE = ca.Sum2(diff)

    return SSE


def RegularizationTerm(sqrtDphiinv):
    """Given sqrt|Dphiinv|, compute the geodesic distance (squared)
    from identity

    """
    ca.SubC_I(sqrtDphiinv, 1.)
    E = ca.Sum2(sqrtDphiinv)
    ca.AddC_I(sqrtDphiinv, 1.)  # reset this to use for later..
    return E


def BodyToStepDir(phiI0, diff, phiinv, sqrtDphiinv, stepdirPhi, stepdirDetDPhi,
                  diffOp, cfIDiff):
    """Convert a body force vector field to an actual search direction

    Outputs are stepdirPhi and stepdirDetDPhi

    """
    grid = phiinv.grid()
    mType = phiinv.memType()

    # start with the ordinary body force (this is the SDiff body force)
    bodyV = ca.ManagedField3D(grid, mType)
    ca.Gradient(bodyV, phiI0)
    ca.Mul_I(bodyV, diff)

    if cfIDiff.symmetric:
        ca.Mul_I(bodyV, sqrtDphiinv)  # mult. by sqrt|Dphi| for sym

    # this is the SDiff variation (negative smoothed body force)
    diffOp.setDivergenceFree(True)
    diffOp.applyInverseOperator(stepdirPhi, bodyV)
    # turn it into the actual SDiff update dir
    ca.MulC_I(stepdirPhi, -cfIDiff.sdiffScaling)

    # laplacian sqrt(|Dphiinv|)
    scratchV = ca.ManagedField3D(grid, mType) # scratch VF to solve Poisson
    ca.SetMem(scratchV, 0.)
    if cfIDiff.symmetric:
        scratchI = ca.ManagedImage3D(grid, mType)
        ca.Sqr(scratchI, diff)
        # a_o = a_i * ca + a_i1 * cb
        ca.MulC_Add_MulC_I(scratchI, 0.5, sqrtDphiinv, 4.)
        ca.AddC_I(scratchI, -4.*cfIDiff.regWeight)
        ca.Mul_I(scratchI, sqrtDphiinv)
        ca.Copy(scratchV, scratchI, 0)
    else:
        ca.MulCAddC(stepdirDetDPhi, sqrtDphiinv,
                    4.*cfIDiff.regWeight, -4.0*cfIDiff.regWeight)

    # Need this for IDiff piece (we'll smooth it after adding the laplacian
    # of sqrt{DPhi} later)
    divbody = ca.ManagedImage3D(grid, mType)
    ca.Divergence(divbody, bodyV)

    # solve biharmonic equation
    ca.Copy(scratchV, divbody, 0)
    del divbody
    diffOp.setDivergenceFree(False)
    diffOp.applyInverseOperator(scratchV)

    # this part impacts the determinant, copy that out now
    smoothdivbody = ca.ManagedImage3D(grid, mType)
    ca.Copy(smoothdivbody, scratchV, 0)
    ca.Add_I(stepdirDetDPhi, smoothdivbody)
    del smoothdivbody

    ca.Copy(scratchV, stepdirDetDPhi, 0)

    # second step in biharmonic solver
    diffOp.applyInverseOperator(scratchV)

    stepf = ca.ManagedImage3D(grid, mType)
    ca.Copy(stepf, scratchV, 0)
    # gradient to get the actual step direction for IDiff part
    ca.Gradient(scratchV, stepf)
    del stepf

    # add IDiff piece (currently in scratchV) to stepdirPhi
    ca.Add_I(stepdirPhi, scratchV)
    del scratchV


def Step(phiI0, diff, phiinv, sqrtDphiinv, diffOp, cfIDiff, cfOptim):
    """Given body force, a vector field, perform a single step using either
    IDiff-only or hybrid gradient descent

    """
    grid = phiI0.grid()
    mType = phiinv.memType()

    # temp variables to hold step direction
    stepdirDetDPhi = ca.ManagedImage3D(grid, mType)
    stepdirPhi = ca.ManagedField3D(grid, mType)

    ca.SetMem(stepdirDetDPhi, 0)
    ca.SetMem(stepdirPhi, 0)

    # after this, gradient direction is in stepdirPhi, stepdirDetDPhi
    BodyToStepDir(phiI0, diff, phiinv, sqrtDphiinv,
                  stepdirPhi, stepdirDetDPhi,
                  diffOp, cfIDiff)

    # Do fixed step-size update
    if cfOptim.method == 'FIXEDGD':
        # compose to update phi
        scratchV = ca.ManagedField3D(grid, mType)
        ca.ComposeHV(scratchV, phiinv, stepdirPhi, cfOptim.stepSize)
        ca.Copy(phiinv, scratchV)
        del scratchV  # be stingy with temp memory

        # update detDphiinv with analytic update
        # multiply by exp(-epsphi g) where g is still in bodyscalar
        ca.MulC_I(stepdirDetDPhi, -0.5*cfOptim.stepSize)
        ca.Exp_I(stepdirDetDPhi)
        ca.Mul_I(sqrtDphiinv, stepdirDetDPhi)  # exponential fall off
        del stepdirDetDPhi

        # deform (Vinv takes a negative step size)
        scratchI = ca.ManagedImage3D(grid, mType)
        ca.ApplyV(scratchI, sqrtDphiinv, stepdirPhi, cfOptim.stepSize)
        ca.Copy(sqrtDphiinv, scratchI)
        del scratchI
    else:
        raise Exception("Unknown optimization scheme: " + cfOptim.method)
